#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

enlace = ""
class myContentHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True

    def endElement (self, name):
        if name == 'item':
            self.inItem = False
        elif self.inItem:
            if name == 'title':
                self.theContent1 = self.theContent
                self.inContent = False
                self.theContent =""
            elif name == 'link':
                global enlace
                self.link = self.theContent
                enlace = enlace + ("<p><a href='" + self.link + "'>"
                         + self.theContent1 + "</a></p>")
                self.inContent = False
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog

def main():
    # Load parser and driver

    theParser = make_parser()
    theHandler = myContentHandler()
    theParser.setContentHandler(theHandler)

    # Ready, set, go!

    xmlFile = urllib.request.urlopen("http://barrapunto.com/barrapunto.rss")
    theParser.parse(xmlFile)

    return ("<html><head><h2> Titulares Barrapunto </h2><body><ul>" +  enlace +
            "</ul></body></head></html>")

if __name__ == "__main__":
    respuesta = main()
    print(respuesta)
