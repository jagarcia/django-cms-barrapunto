from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from .models import Page
from . import parserbarrapunto

update = parserbarrapunto.main()

formulario = '''
<form action="" method = "POST">
     <br>
     <input type = "text" name = "content" value= ""><br><br>
     <input type = "submit" value = "Enviar">
</form>
'''
html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

actualizar = "<h2> Si quiere actualizar el contenido rellene el formulario"
rellena = "</h1><h2> Rellena el formulario.: </h2>"
actualizado = "<h1> ACTUALIZADO </h1><h2> Realiza un / para ver la lista</h2>"

parseador = str(parserbarrapunto.main())

def show_list(request):
    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return(HttpResponse(html_template.format(body=body)))

@csrf_exempt
def index(request, name):
    if request.method == "POST":
        try:
            page = Page.objects.get(name = name)
            page.content = request.POST['content']
        except Page.DoesNotExist:
            page = Page(name = name, content = request.POST['content'])
        page.save()
        if page.content == "":
            return HttpResponse("<h1>Debe introducir contenido</h1>"
                        + formulario)
        else:
            return HttpResponse(actualizado  + parseador)

    elif request.method == "GET":
        try:
            page = Page.objects.get(name = name)
            return HttpResponse("<h1>Contenido.:" + page.content + "</h1>"+
                        actualizar + formulario + parseador)
        except Page.DoesNotExist:
            return HttpResponse("<h1>Pagina no encontrada.:" + name +
                        rellena + formulario + parseador)
    else:
        return HttpResponse("metodo invalido")
